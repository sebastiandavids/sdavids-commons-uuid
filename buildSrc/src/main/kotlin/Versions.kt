/*
 * Copyright (c) 2019, Sebastian Davids
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.gradle.api.JavaVersion.VERSION_1_8

object Versions {

  // Languages
  val jvmTarget = VERSION_1_8

  // Dependencies
  val apiGuardian = "1.0.0"
  val jsr305 = "3.0.2"

  // Test Dependencies
  val junit = "5.4.0"
  val truth = "0.42"
  val sdavidsCommonsTest = "2.0.2"

  // Tools
  val gradle = "5.2.1"
  val errorprone = "2.3.2"
  val errorproneJavac = "9+181-r4173-1"
  val checkstyle = "8.17"
  val pmd = "6.11.0"
  val spotbugs = "3.1.11"
  val findsecbugs = "1.8.0"
  val java8Signature = "1.0"
  val jacoco = "0.8.3"
  val googleCodeStyle = "1.7"
  val ktlint = "0.30.0"
  val checkerFramework = "2.5.5"
}
